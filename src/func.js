const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string') return false;
  try {
    const bigint1 = BigInt(str1);
    const bigint2 = BigInt(str2);
    return (bigint1 + bigint2).toString();
  } catch {
    return false;
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const author = listOfPosts.reduce((acc, currPost) => {
    if(currPost.author === authorName) {
      acc.posts++;
    }
    currPost.comments?.forEach(comment => {
      if(comment.author === authorName) {
        acc.comments++;
      }
    });
    
    return acc;
  }, { posts: 0, comments: 0 });

  return `Post:${author.posts},comments:${author.comments}`;
};

const tickets=(people)=> {
  const cash = {'25': 0, '50': 0};
  for (const person of people) {
    if (person === 25) {
      cash['25']++;
    } else if (person === 50) {
      if (cash['25']) {
        cash['25']--;
        cash['50']++;
      } else {
        return 'NO';
      }
    } else if (person === 100) {
      if (cash['50'] && cash['25']) {
        cash['50']--;
        cash['25']--;
      } else if (cash['25'] >= 3) {
        cash['25']-= 3;
      } else {
        return 'NO';
      }
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
